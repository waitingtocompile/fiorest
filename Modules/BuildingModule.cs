﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class BuildingModule : NancyModule
    {
        public BuildingModule() : base("/building")
        {
            Post("/", _ =>
            {
                this.EnforceAuth();
                return PostBuilding();
            });

            Get("/{building_ticker}", parameters =>
            {
                return GetBuilding(parameters.building_ticker);
            });
        }

        private Response PostBuilding()
        {
            using (var req = new FIORequest<JSONRepresentations.WorldReactorData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                BUIModel model = req.DB.BUIModels.Where(b => b.Ticker == data.ticker).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new BUIModel();
                }
                else
                {
                    model.BuildingCosts.Clear();
                    model.Recipes.Clear();
                }

                model.Name = data.name;
                model.Ticker = data.ticker;
                model.Expertise = data.expertise;

                foreach (var workforceCapacity in data.workforceCapacities)
                {
                    switch (workforceCapacity.level)
                    {
                        case "PIONEER":
                            model.Pioneers = workforceCapacity.capacity;
                            break;
                        case "SETTLER":
                            model.Settlers = workforceCapacity.capacity;
                            break;
                        case "TECHNICIAN":
                            model.Technicians = workforceCapacity.capacity;
                            break;
                        case "ENGINEER":
                            model.Engineers = workforceCapacity.capacity;
                            break;
                        case "SCIENTIST":
                            model.Scientists = workforceCapacity.capacity;
                            break;
                    }
                }

                model.AreaCost = data.areaCost;

                foreach (var buildingCost in data.buildingCosts)
                {
                    BUIBuildingCost buiCost = new BUIBuildingCost();

                    buiCost.CommodityName = buildingCost.material.name;
                    buiCost.CommodityTicker = buildingCost.material.ticker;
                    buiCost.Weight = buildingCost.material.weight;
                    buiCost.Volume = buildingCost.material.volume;
                    buiCost.Amount = buildingCost.amount;

                    model.BuildingCosts.Add(buiCost);
                }

                foreach (var recipe in data.recipes)
                {
                    BUIRecipe buiRecipe = new BUIRecipe();

                    foreach (var input in recipe.inputs)
                    {
                        BUIRecipeInput buiInput = new BUIRecipeInput();

                        buiInput.CommodityName = input.material.name;
                        buiInput.CommodityTicker = input.material.ticker;
                        buiInput.Weight = input.material.weight;
                        buiInput.Volume = input.material.volume;
                        buiInput.Amount = input.amount;

                        buiRecipe.Inputs.Add(buiInput);
                    }

                    foreach (var output in recipe.outputs)
                    {
                        BUIRecipeOutput buiOutput = new BUIRecipeOutput();

                        buiOutput.CommodityName = output.material.name;
                        buiOutput.CommodityTicker = output.material.ticker;
                        buiOutput.Weight = output.material.weight;
                        buiOutput.Volume = output.material.volume;
                        buiOutput.Amount = output.amount;

                        buiRecipe.Outputs.Add(buiOutput);
                    }

                    buiRecipe.DurationMs = recipe.duration.millis;

                    model.Recipes.Add(buiRecipe);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.BUIModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetBuilding(string buildingTicker)
        {
            buildingTicker = buildingTicker.ToUpper();
            using (var DB = new PRUNDataContext())
            {
                var res = DB.BUIModels.Where(b => b.Ticker == buildingTicker).FirstOrDefault();
                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }
    }
}
