﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class UserModule : NancyModule
    {
        public UserModule() : base("/user")
        {
            this.EnforceAuth();

            Post("/", _ =>
            {
                return PostUser();
            });

            Get("/{username}", parameters =>
            {
                return GetUser(parameters.username);
            });

            Post("/resetalldata", _ =>
            {
                return PostResetAllData();
            });
        }

        private Response PostUser()
        {
            using (var req = new FIORequest<JSONRepresentations.UserData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                var model = req.DB.UserDataModels.Where(c => c.CompanyId == data.id).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new UserDataModel();
                }

                model.UserName = data.username;
                model.Tier = data.highestTier;

                model.Team = data.team;
                model.Pioneer = data.pioneer;

                model.SystemNamingRights = data.systemNamingRights;
                model.PlanetNamingRights = data.planetNamingRights;

                model.IsPayingUser = data.isPayingUser;
                model.IsModeratorChat = data.isModeratorChat;

                model.CreatedEpochMs = data.created.timestamp;

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.UserDataModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetUser(string UserName)
        {
            UserName = UserName.ToUpper();
            using (var DB = new PRUNDataContext())
            {
                var res = DB.UserDataModels.Where(u => u.UserName.ToUpper() == UserName).FirstOrDefault();
                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response PostResetAllData()
        {
            string UserName = Request.GetUserName().ToUpper();

            using (var DB = new PRUNDataContext())
            using (var transaction = DB.Database.BeginTransaction())
            {
                DB.CompanyDataModels.RemoveRange(DB.CompanyDataModels.Where(c => c.UserNameSubmitted.ToUpper() == UserName));
                DB.PRODLinesModels.RemoveRange(DB.PRODLinesModels.Where(p => p.UserNameSubmitted.ToUpper() == UserName));
                DB.SHIPSModels.RemoveRange(DB.SHIPSModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.SITESModels.RemoveRange(DB.SITESModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.StorageModels.RemoveRange(DB.StorageModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));
                DB.WorkforceModels.RemoveRange(DB.WorkforceModels.Where(s => s.UserNameSubmitted.ToUpper() == UserName));

                DB.SaveChanges();
                transaction.Commit();
                return HttpStatusCode.OK;
            }
        }
    }
}
