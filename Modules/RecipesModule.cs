﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using System.Collections.Generic;

namespace FIORest.Modules
{
    public class RecipesModule : NancyModule
    {
        public RecipesModule() : base("/recipes")
        {
            Get("/", _ =>
            {
                return GetRecipes();
            });

            //Get("/input/{input}", parameters =>
            //{
            //    return GetRecipesByInput(parameters.input);
            //});
            //
            //Get("/output/{output}", parameters =>
            //{
            //    return GetRecipesByOutput(parameters.output);
            //});
        }

        class MinimalInput
        {
            public string Ticker { get; set; }
            public int Amount { get; set; }
        }

        class MinimalOutput
        {
            public string Ticker { get; set; }
            public int Amount { get; set; }
        }

        class MinimalRecipe
        {
            public List<MinimalInput> Inputs { get; set; } = new List<MinimalInput>();
            public List<MinimalOutput> Outputs { get; set; } = new List<MinimalOutput>();
            public int TimeMs { get; set; }
        }

        private Response GetRecipes()
        {
            using(var DB = new PRUNDataContext())
            {
                List<List<BUIRecipe>> res = DB.BUIModels.Select(b => b.Recipes).ToList();

                List<MinimalRecipe> minimalRecipes = new List<MinimalRecipe>();
                foreach(var listRecipes in res)
                {
                    foreach(var recipe in listRecipes)
                    {
                        MinimalRecipe mr = new MinimalRecipe();

                        foreach (var input in recipe.Inputs)
                        {
                            MinimalInput mi = new MinimalInput();
                            mi.Ticker = input.CommodityTicker;
                            mi.Amount = input.Amount;
                            mr.Inputs.Add(mi);
                        }

                        foreach (var output in recipe.Outputs)
                        {
                            MinimalOutput mo = new MinimalOutput();
                            mo.Ticker = output.CommodityTicker;
                            mo.Amount = output.Amount;
                            mr.Outputs.Add(mo);
                        }

                        mr.TimeMs = recipe.DurationMs;
                        minimalRecipes.Add(mr);
                    }
                }

                return JsonConvert.SerializeObject(minimalRecipes);
            }
        }

        //private Response GetRecipesByInput(string input)
        //{
        //    return null;
        //}
        //
        //private Response GetRecipesByOutput(string output)
        //{
        //    return null;
        //}
    }
}
