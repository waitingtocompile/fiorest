﻿using System;
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class ExchangeModule : NancyModule
    {
        public ExchangeModule() : base("/exchange")
        {
            Post("/", _ =>
            {
                this.EnforceAuth();
                return PostExchange();
            });

            Get("/{exchange_ticker}", parameters =>
            {
                return GetExchange(parameters.exchange_ticker);
            });
        }

        private Response PostExchange()
        {
            using (var req = new FIORequest<JSONRepresentations.ComexBrokerData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;

                var model = req.DB.CXDataModels.Where(c => c.MaterialTicker == data.material.ticker && c.ExchangeCode == data.exchange.code).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new CXDataModel();
                }
                else
                {
                    model.BuyingOrders.Clear();
                    model.SellingOrders.Clear();
                }

                model.MaterialName = data.material.name;
                model.MaterialTicker = data.material.ticker;
                model.MaterialId = data.material.id;

                model.ExchangeName = data.exchange.name;
                model.ExchangeCode = data.exchange.code;

                model.Currency = data.currency.code;

                model.Previous = data.previous?.amount;
                model.Price = data.price?.amount;
                model.PriceTimeEpochMs = data.priceTime?.timestamp;

                model.High = data.high?.amount;
                model.AllTimeHigh = data.allTimeHigh?.amount;

                model.Low = data.low?.amount;
                model.AllTimeLow = data.allTimeLow?.amount;

                model.Ask = data.ask?.price?.amount;
                model.AskCount = data.ask?.amount;

                model.Bid = data.bid?.price?.amount;
                model.BidCount = data.bid?.amount;

                model.Supply = data.supply;
                model.Demand = data.demand;
                model.Traded = data.traded;

                model.VolumeAmount = data.volume?.amount;

                model.PriceAverage = data.priceAverage?.amount;

                model.NarrowPriceBandLow = data.narrowPriceBand?.low;
                model.NarrowPriceBandHigh = data.narrowPriceBand?.high;

                model.WidePriceBandLow = data.widePriceBand?.low;
                model.WidePriceBandHigh = data.widePriceBand?.high;

                foreach (var sellingOrder in data.sellingOrders)
                {
                    CXSellOrder sellOrder = new CXSellOrder();

                    sellOrder.CompanyId = sellingOrder.trader.id;
                    sellOrder.CompanyName = sellingOrder.trader.name;
                    sellOrder.CompanyCode = sellingOrder.trader.code;
                    sellOrder.ItemCount = sellingOrder.amount;
                    sellOrder.ItemCost = sellingOrder.limit.amount;

                    model.SellingOrders.Add(sellOrder);
                }

                foreach (var buyingOrder in data.buyingOrders)
                {
                    CXBuyOrder buyOrder = new CXBuyOrder();

                    buyOrder.CompanyId = buyingOrder.trader.id;
                    buyOrder.CompanyName = buyingOrder.trader.name;
                    buyOrder.CompanyCode = buyingOrder.trader.code;
                    buyOrder.ItemCount = buyingOrder.amount;
                    buyOrder.ItemCost = buyingOrder.limit.amount;

                    model.BuyingOrders.Add(buyOrder);
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.CXDataModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetExchange(string ExchangeTicker)
        {
            string[] parts = ExchangeTicker.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if ( parts.Length == 2)
            {
                string materialCode = parts[0].ToUpper();
                string exchangeCode = parts[1].ToUpper();
                using (var DB = new PRUNDataContext())
                {
                    var res = DB.CXDataModels.Where(c => c.MaterialTicker == materialCode && c.ExchangeCode == exchangeCode).FirstOrDefault();
                    if ( res != null )
                    {
                        return JsonConvert.SerializeObject(res);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }
            else
            {
                Response badRequest = "Invalid ticker format.";
                badRequest.StatusCode = HttpStatusCode.BadRequest;
                return badRequest;
            }
        }
    }
}
