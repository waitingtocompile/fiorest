﻿using System;
using System.Collections.Generic;
using System.IO;

using Newtonsoft.Json;

namespace FIORest
{
    public enum LogLevel
    {
        Info,
        Warning,
        Error
    }

    public static class Logger
    {
        public static void Log(string message)
        {
            Log(LogLevel.Info, message);
        }

        public static void Log(LogLevel logLevel, string message)
        {
            switch(logLevel)
            {
                case LogLevel.Info:
                    Console.Out.WriteLine($"\t{message}");
                    break;
                case LogLevel.Warning:
                    Console.Error.WriteLine($"\tWARNING: {message}");
                    break;
                case LogLevel.Error:
                    Console.Error.WriteLine($"\tERROR: {message}");
                    break;
            }
        }

        public static void LogBadRequest(string body, string UserName, Dictionary<string,string> ExData)
        {
            if (String.IsNullOrWhiteSpace(UserName))
            {
                UserName = "UnknownUserName";
            }

            Directory.CreateDirectory(Globals.Opts.BadRequestPath);
            string UserBadRequestsPath = Path.Combine(Globals.Opts.BadRequestPath, UserName);
            Directory.CreateDirectory(UserBadRequestsPath);
            string TimeBadRequestPath = Path.Combine(UserBadRequestsPath, $"{DateTime.Now.Ticks}-{Path.GetRandomFileName()}");
            Directory.CreateDirectory(TimeBadRequestPath);
            File.WriteAllText(Path.Combine(TimeBadRequestPath, "ExData.json"), JsonConvert.SerializeObject(ExData, Formatting.Indented));
            File.WriteAllText(Path.Combine(TimeBadRequestPath, "Request.json"), body);
        }
    }
}
