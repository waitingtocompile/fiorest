﻿# Admin
## POST: /admin/create [RequiresAdminAuth]
Creates a User

### Payload
```
{
	"UserName": "Joe",
	"Password": "Password",
	"IsAdmin": false
}
```
### Result
OK

## POST: /admin/disable [RequiresAdminAuth]
Disables the account prevent authentication.

### Payload
```
{
	"UserName": "Joe"
}
```
### Result
OK if user was disabled.  BadRequest otherwise.

# Auth
## GET: /auth
Returns OK if authenticated (appropriate headers), forbidden otherwise.

## POST: /auth/login
Logs in.
### Payload
```
{
	"UserName": "Joe",
	"Password": "Password"
}
```
### Response
```
{
	"AuthToken": "AUTH_TOKEN",
	"Expiry": "UTC_EXPIRATION"
}
```
OK on success.  Unauthorized otherwise.

## POST: /auth/changepassword [RequiresAuth]
Changes the password.

### Payload
```
{
	"OldPassword": "Password",
	"NewPassword": "OMGWTFBBQ"
}
```
### Response
OK on success.  Unauthorized otherwise.

## POST: /auth/addpermission [RequiresAuth]
Adds a permission.  UserName of "\*" is considered a wildcard for anyone.

### Payload
```
{
	"UserName": "OtherUser",
	"FlightData": true,
	"BuildingData": false,
	"StorageData": false,
	"ProductionData": true,
	"WorkforceData": true,
	"ExpertsData": true
}
```
### Response
OK on success.  NoContent if user is not found.  Unauthorized otherwise.

## GET: /auth/permissions [RequiresAuth]
Retreives the permission table for the logged in user.

## POST: /auth/deletepermisson/{username} [RequiresAuth]
Deletes the permission entry for the user

### Response
```
[
	{
		"UserName": "OtherUser",
		"FlightData": true,
		"BuildingData": false,
		"StorageData": false,
		"ProductionData": true,
		"WorkforceData": true,
		"ExpertsData": true
	},
	...
]
```

# Building
## POST: /building [RequiresAuth]
Submits a BUI to the database.

## GET: /building/{ticker}
Retrieves a BUI definition.

# Company
## POST /company [RequiresAuth]
Submits company information to the database.

## POST /company/data [RequiresAuth]
Submits company_data (another object) information to the database.

## GET /company/{id} [RequiresAuth]
Retrieves company information.

# Exchange
## POST /exchange [RequiresAuth]
Submits exchange data.

## GET /exchange/{ticker}
Retreives CX data.

# Infrastructure
## POST /infrastructure [RequiresAuth]
Submits infrastructure data.

## GET /infrastructure/{planet_or_infrastructure_id}
Retrieves infrastructure data.  PlanetId, PlanetNaturalId, PlanetName all valid inputs.

# LocalMarket
## POST /localmarket [RequiresAuth]
Submits localmarket data.

## GET /localmarket/{localmarketid}
Retrieves the localmarket data given an id.

## GET /localmarket/{planet}
Retrieves the localmarket data given a PlanetId, PlanetNaturalId, or PlanetName.

## GET /localmarket/{planet}/{type}
Retrieves the type-specifiy localmarket data given a PlanetId, PlanetNaturalId, or PlanetName.
Valid Types:
- BUY/BUYS/BUYING
- SELL/SELLS/SELLING
- SHIP/SHIPPING

## GET /localmarket/shipping/source/{planet}
Finds shipping contracts with the source at a given PlanetId, PlanetNaturalId, or PlanetName.

## GET /localmarket/shipping/destination/{planet}
Find shipping contracts with the destination at a given PlanetId, PlanetNaturalId, or PlanetName.

# Material
## POST /material [RequiresAuth]
Submits material data.

## GET /material/{ticker}
Retrieves material info for the given ticker.

# Planet
## POST /planet [RequiresAuth]
Submits planet data.

## GET /planet/{planet}
Retrieves planet data for the given PlanetId, PlanetNaturalId, or PlanetName.

## POST /planet/search [RequiresAuth]
Retrieves planet data for the given sample payload:
```
{
	"Materials":
	[
		"HE3",
		"NE"
	],
	"IncludeRocky":true,
	"IncludeGaseous":true,
	"IncludeLowGravity":true,
	"IncludeHighGravity":true,
	"IncludeLowPressure":false,
	"IncludeHighPressure":true,
	"IncludeLowTemperature":true,
	"IncludeHighTempearture":false
	"MustBeFertile":false
}
```

# Production
## POST /production [RequiresAuth]
Submits production information.

## GET /production/{username} [RequiresAuth]
Retrieves all production information for the given username.

## GET /production/{username}/{planet} [RequiresAuth]
Retrieves production information for the given username and PlanetId, PlanetNaturalId, or PlanetName.

## GET /production/planets/{username} [RequiresAuth]
Retrieves production planets for the given user.

# Flight

@TODO: FIX ME

# Sites
## POST /sites [RequiresAuth]
Submits sites information.

## GET /sites/{username} [RequiresAuth]
Retrieves the site information for the provided username.

## GET /sites/{username}/{planet}
Retrieves the site information for the provided username and PlanetId, PlanetNaturalId, or PlanetName.

## GET /sites/planets/{username}
Retrieves site planets for the given user.

# Storage
## POST /storage [RequiresAuth]
Submits storage information.

## POST /storage/change [RequiresAuth]
Submits storage change information.

## GET /storage/{username} [RequiresAuth]
Retrieves storage information for the given username.

## GET /storage/{username}/{planet} [RequiresAuth]
Retrieves storage information for the given username and PlanetId, PlanetNaturalId, or PlanetName.

## GET /storage/planets/{username} [RequiresAuth]
Retrieves storage planets for the given user.

# User
## POST /user [RequiresAuth]
Submits user infromation.

## GET /user/{username} [RequiresAuth]
Retrieves user information given a username.

# Workforce
## POST /workforce [RequiresAuth]
Submits workforce information.

## GET /workforce/{username} [RequiresAuth]
Retrieves workforce information for the username.

## GET /workforce/{username}/{planet} [RequiresAuth]
Retrieves workforce information for the username and PlanetId, PlanetNaturalId, or PlanetName.

## GET /workforce/planets/{username} [RequiresAuth]
Retrieves workforce planets for the given user.