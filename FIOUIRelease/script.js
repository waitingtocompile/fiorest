//var fnar_username = "";
//var fnar_password = "";

var fnar_url = "https://rest.fnar.net";

//
// functions to get logged in & send data.
//
var fnar_auth_token = "";
var fnar_known_auth_failed = false;

function fnar_login_then(turl, tdata)
{
    if (fnar_known_auth_failed)
    {
        return;
    }

    var data = {
        "UserName": fnar_username,
        "Password": fnar_password
    };

    var url = fnar_url + "/auth/login";
    var fnarhttp = new XMLHttpRequest();

    fnarhttp.onreadystatechange = function()
    {
        if (this.readyState === XMLHttpRequest.DONE)
        {
            var status = this.status;
            if (status === 0 || (status >= 200 && status < 400))
            {
                // The request has been completed successfully
                console.log("Server response (login then do):");
                console.log(this.response);
                var json = JSON.parse(this.response);
                fnar_auth_token = json.AuthToken;
                fnar_do_send_xhttp_request(turl, tdata);
            }
            else if (status === 401)
            {
                fnar_known_auth_failed = true;
                alert("Authentication to FNAR data loader failed.  Check your username & password.");
            }
            else
            {
				alert("Logged in.")
            }
        }
    };
    fnarhttp.withCredentials = false;
    fnarhttp.open("POST", url, true);
    fnarhttp.setRequestHeader("Content-type", "application/json");
    fnarhttp.send(JSON.stringify(data));
    console.log("Attempting to sign in.");
}

function fnar_renew_or_reauth_then(url, data)
{
    fnar_login_then(url, data);
}

function send_fnar_xhttp_request(url, data)
{
	if(fnar_auth_token == "")
	{
		fnar_login_then(url, data);
	}
	else
	{
		fnar_do_send_xhttp_request(url, data);
	}

}

function fnar_do_send_xhttp_request(url, data)
{
	if ( fnar_auth_token === "")
		return;
	
    var fnarhttp = new XMLHttpRequest();
    fnarhttp.onreadystatechange = function()
    {
        if (this.readyState === XMLHttpRequest.DONE)
        {
            var status = this.status;
            if (status == 401)
            {
                fnar_renew_or_reauth_then(url, data);
            }
            if (status === 0 || (status >= 200 && status < 400))
            {
                // The request has been completed successfully
                // console.log("Server response (fnar_do_send_xhttp_request):");
                // console.log(this);
            }
            else
            {
                // Oh no! There has been an error with the request!
            }
        }
    };
    // console.log(fnar_auth_token);
    fnarhttp.withCredentials = false;
    fnarhttp.open("POST", url, true);
    fnarhttp.setRequestHeader("Authorization", fnar_auth_token);
    fnarhttp.setRequestHeader("Content-type", "application/json");
	if (data != null)
	{
		fnarhttp.send(JSON.stringify(data));
	}
}


(function()
{
    'use strict';

    var transmitted_events = [
        'PLANET_DATA_DATA',
        'INFRASTRUCTURE_DATA_DATA',
        'COMPANY_DATA_DATA',
        'LOCAL_MARKET_DATA_DATA',
        'COMPANY_DATA',
        'COMEX_BROKER_DATA',
		'SHIP_SHIPS',
        'SHIP_FLIGHT_FLIGHTS',
        'WORLD_MATERIAL_CATEGORIES',
		'WORLD_REACTOR_DATA',
        'SITE_SITES',
        'STORAGE_STORAGES',
        'STORAGE_CHANGE',
        'USER_DATA',
        'PRODUCTION_SITE_PRODUCTION_LINES',
        'WORKFORCE_WORKFORCES',
        //'PLANET_SITES',
        //'SYSTEM_STARS_DATA',
    ];

    var OrigWebSocket = window.WebSocket;
    var callWebSocket = OrigWebSocket.apply.bind(OrigWebSocket);
    var wsAddListener = OrigWebSocket.prototype.addEventListener;
    wsAddListener = wsAddListener.call.bind(wsAddListener);
    window.WebSocket = function WebSocket(url, protocols)
    {	
        var ws;
        if (!(this instanceof WebSocket))
        {
            // Called without 'new' (browsers will throw an error).
            ws = callWebSocket(this, arguments);
        }
        else if (arguments.length === 1)
        {
            ws = new OrigWebSocket(url);
        }
        else if (arguments.length >= 2)
        {
            ws = new OrigWebSocket(url, protocols);
        }
        else
        {
            ws = new OrigWebSocket();
        }

        wsAddListener(ws, 'message', function(event)
        {
            var outmsg = '';
            // Do stuff with event.data (received data).
            var re_event = /^[0-9:\s]*(?<event>\[\s*"event".*\])[\s0-9:\.]*/m;
            var result = event.data.match(re_event);
            if (result && result.groups && result.groups.event)
            {
                console.log("Event found");
                var eventdata = JSON.parse(result.groups.event)[1];
                console.log(eventdata);
                if ((eventdata.messageType == "ACTION_COMPLETED" && eventdata.payload.message) || (eventdata.messageType == "STORAGE_CHANGE" && eventdata.payload))
                {
                    var msgType = (eventdata.messageType == "ACTION_COMPLETED") ? eventdata.payload.message.messageType : eventdata.messageType;
                    if (msgType == "DATA_DATA")
                    {
                        if ( eventdata.payload.message.payload.body.planetId && !eventdata.payload.message.payload.body.plot )
                        {
                            msgType = "PLANET_DATA_DATA";
                        }
                        else if (eventdata.payload.message.payload.body.company)
                        {
                            msgType = "COMPANY_DATA_DATA";
                        }
                        else if (eventdata.payload.message.payload.body.infrastructure)
                        {
                            msgType = "INFRASTRUCTURE_DATA_DATA";
                        }
                        else if (eventdata.payload.message.payload.path && eventdata.payload.message.payload.path.length == 3 && eventdata.payload.message.payload.path[2] == "ads")
                        {
                            msgType = "LOCAL_MARKET_DATA_DATA";
                        }
                    }

                    if (transmitted_events.includes(msgType))
                    {
                        switch(msgType)
                        {
                            case "WORLD_REACTOR_DATA":
                                send_fnar_xhttp_request(fnar_url + "/building", eventdata);
                                break;
                            case "WORLD_MATERIAL_CATEGORIES":
                                send_fnar_xhttp_request(fnar_url + "/material", eventdata);
                                break;
							case "SHIP_SHIPS":
								send_fnar_xhttp_request(fnar_url + "/ship/ships", eventdata);
								break;
                            case "SHIP_FLIGHT_FLIGHTS":
                                send_fnar_xhttp_request(fnar_url + "/ship/flights", eventdata);
                                break;
                            case "SITE_SITES":
                                send_fnar_xhttp_request(fnar_url + "/sites", eventdata);
                                break;
                            case "STORAGE_STORAGES":
                                send_fnar_xhttp_request(fnar_url + "/storage", eventdata);
                                break;
                            case "STORAGE_CHANGE":
                                send_fnar_xhttp_request(fnar_url + "/storage/change", eventdata);
                                break;
                            case "COMEX_BROKER_DATA":
                                send_fnar_xhttp_request(fnar_url + "/exchange", eventdata);
                                break;
                            case "COMPANY_DATA":
                                send_fnar_xhttp_request(fnar_url + "/company", eventdata);
                                break;
                            case "USER_DATA":
                                send_fnar_xhttp_request(fnar_url + "/user", eventdata);
                                break;
                            case "PLANET_DATA_DATA":
                                send_fnar_xhttp_request(fnar_url + "/planet", eventdata);
                                break;
                            case "COMPANY_DATA":
                                send_fnar_xhttp_request(fnar_url + "/company", eventdata);
                                break;
                            case "COMPANY_DATA_DATA":
                                send_fnar_xhttp_request(fnar_url + "/company/data", eventdata);
                                break;
                            case "INFRASTRUCTURE_DATA_DATA":
                                send_fnar_xhttp_request(fnar_url + "/infrastructure", eventdata);
                                break;
                            case "LOCAL_MARKET_DATA_DATA":
                                send_fnar_xhttp_request(fnar_url + "/localmarket", eventdata);
                                break;
                            case "PRODUCTION_SITE_PRODUCTION_LINES":
                                send_fnar_xhttp_request(fnar_url + "/production", eventdata);
                                break;
                            case "WORKFORCE_WORKFORCES":
                                send_fnar_xhttp_request(fnar_url + "/workforce", eventdata);
                                break;
                        }
                    }
                    else
                    {
                        // console.log("Uninterested in action message: " + eventdata.payload.message.messageType);
                    }
                }
            }
        });
        return ws;
    }.bind();
	
    window.WebSocket.prototype = OrigWebSocket.prototype;
    window.WebSocket.prototype.constructor = window.WebSocket;

    var wsSend = OrigWebSocket.prototype.send;
    wsSend = wsSend.apply.bind(wsSend);
    OrigWebSocket.prototype.send = function(data)
    {
        return wsSend(this, arguments);
    };
})();