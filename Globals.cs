﻿using FIORest.Database;

namespace FIORest
{
    public static class Globals
    {
        public static Options Opts = new Options();

        public static string ConnectionString
        {
            get
            {
                return $"Data Source={Globals.Opts.DatabaseFilePath}";
            }
        }
    }
}
