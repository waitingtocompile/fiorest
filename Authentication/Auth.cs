﻿//#define DISABLE_AUTH

using System;
using System.Linq;

using Nancy;

using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Authentication
{
    public static class Auth
    {
        public static bool IsAuthenticated(this Request request)
        {
            return request.EnforceAuthInner() == null;
        }

        public static string GetUserName(this Request request)
        {
            if (!request.Headers.Keys.Contains("Authorization"))
            {
                return null;
            }

            string AuthorizationStr = request.Headers["Authorization"].ToList()[0];
            Guid AuthorizationGuid;
            if ( Guid.TryParse(AuthorizationStr, out AuthorizationGuid))
            {
                using (var DB = new PRUNDataContext())
                {
                    var result = DB.AuthenticationModels.Where(e => e.AuthorizationKey == AuthorizationGuid).FirstOrDefault();
                    if (result != null)
                    {
                        return result.UserName.ToUpper();
                    }
                }
            }

            return null;
        }

        public static string GetAuthToken(this Request request)
        {
            if ( !request.Headers.Keys.Contains("Authorization"))
            {
                return null;
            }

            return request.Headers["Authorization"].ToList()[0];
        }

        private static Response EnforceAuthInner(this Request request, bool RequireAdmin = false)
        {
#if DISABLE_AUTH
            return null;
#else
            bool bTriedAdminAndFailed = false;
            bool bContainsAutorization = request.Headers.Keys.ToList().Contains("Authorization");
            if (bContainsAutorization)
            {
                DateTime now = DateTime.Now.ToUniversalTime();

                Guid Authorization;
                if (Guid.TryParse(request.Headers["Authorization"].ToList()[0], out Authorization))
                {
                    AuthenticationModel result = null;
                    using (var DB = new PRUNDataContext())
                    {
                        
                        if (RequireAdmin)
                        {
                            result = DB.AuthenticationModels.Where(e => e.AuthorizationKey == Authorization && now < e.AuthorizationExpiry && e.IsAdministrator == true).FirstOrDefault();
                            if (result == null && DB.AuthenticationModels.Where(e => e.AuthorizationKey == Authorization && now < e.AuthorizationExpiry && e.AccountEnabled).FirstOrDefault() != null)
                            {
                                bTriedAdminAndFailed = true;
                            }
                        }
                        else
                        {
                            result = DB.AuthenticationModels.Where(e => e.AuthorizationKey == Authorization && now < e.AuthorizationExpiry && e.AccountEnabled).FirstOrDefault();
                        }
                    }
                    
                    if (result != null)
                    {
                        return null;
                    }
                }
            }

            string message = "You are not logged in or your ticket expired.";
            if ( bTriedAdminAndFailed )
            {
                message = "You are not an administrator. Attempt logged.";
            }

            Response resp = message;
            resp.StatusCode = HttpStatusCode.Unauthorized;
            return resp;
#endif
        }

        public enum PrivacyType
        {
            Flight,
            Building,
            Storage,
            Production,
            Workforce,
            Experts
        }

        private static bool CheckPrivacyType(PermissionAllowance permissionAllowance, PrivacyType privacyType)
        {
            if (permissionAllowance != null)
            {
                switch(privacyType)
                {
                    case PrivacyType.Flight:
                        return permissionAllowance.FlightData;
                        
                    case PrivacyType.Building:
                        return permissionAllowance.BuildingData;
                        
                    case PrivacyType.Storage:
                        return permissionAllowance.StorageData;
                        
                    case PrivacyType.Production:
                        return permissionAllowance.ProductionData;
                        
                    case PrivacyType.Workforce:
                        return permissionAllowance.WorkforceData;
                        
                    case PrivacyType.Experts:
                        return permissionAllowance.ExpertsData;
                }
            }

            return false;
        }

        public static bool CanSeeData(string RequesterUserName, string AccessUserName, PrivacyType privacyType)
        {
            RequesterUserName = RequesterUserName.ToUpper();
            AccessUserName = AccessUserName.ToUpper();

#if DISABLE_AUTH
            return true;
#else
            if ( RequesterUserName == AccessUserName)
            {
                // Everyone has permission to see their own data
                return true;
            }

            using (var DB = new PRUNDataContext())
            {
                AuthenticationModel accessModel = DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == AccessUserName).FirstOrDefault();
                if (accessModel != null)
                {
                    // First check for "any user" entries
                    PermissionAllowance permissionAllowance = accessModel.Allowances.Where(a => a.UserName == "*").FirstOrDefault();
                    if (CheckPrivacyType(permissionAllowance, privacyType))
                    {
                        return true;
                    }

                    permissionAllowance = accessModel.Allowances.Where(a => a.UserName.ToUpper() == RequesterUserName).FirstOrDefault();
                    if (CheckPrivacyType(permissionAllowance, privacyType))
                    {
                        return true;
                    }
                }

                return false;
            }
#endif
        }


        public static void EnforceAuthAdmin(this NancyModule module)
        {
            module.Before += ctx =>
            {
                return ctx.Request.EnforceAuthInner(true);
            };
        }

        public static void EnforceAuth(this NancyModule module)
        {
            module.Before += ctx =>
            {
                return ctx.Request.EnforceAuthInner();
            };
        }
    }
}
