﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class SITESModel
    {
        [JsonIgnore]
        public int SITESModelId { get; set; }

        public virtual List<SITESSite> Sites { get; set; } = new List<SITESSite>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SITESSite
    {
        [JsonIgnore]
        public int SITESSiteId { get; set; }

        public string SiteId { get; set; }

        public string PlanetId { get; set; }
        public string PlanetIdentifier { get; set; }
        public string PlanetName { get; set; }

        public long PlanetFoundedEpochMs { get; set; }

        public virtual List<SITESBuilding> Buildings { get; set; } = new List<SITESBuilding>();

        [JsonIgnore]
        public int SITESModelId { get; set; }
        [JsonIgnore]
        public virtual SITESModel SITESModel { get; set; }
    }

    public class SITESBuilding
    {
        [JsonIgnore]
        public int SITESBuildingId { get; set; }

        public string BuildingName { get; set; }
        public string BuildingTicker { get; set; }

        public double Condition { get; set; }
        public virtual List<SITESReclaimableMaterial> ReclaimableMaterials { get; set; } = new List<SITESReclaimableMaterial>();
        public virtual List<SITESRepairMaterial> RepairMaterials { get; set; } = new List<SITESRepairMaterial>();

        [JsonIgnore]
        public int SITESSiteId { get; set; }
        [JsonIgnore]
        public virtual SITESSite SITESSite { get; set; }
    }

    public class SITESReclaimableMaterial
    {
        [JsonIgnore]
        public int SITESReclaimableMaterialId { get; set; }

        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }

        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public int SITESBuildingId { get; set; }
        [JsonIgnore]
        public virtual SITESBuilding SITESBuilding { get; set; }
    }

    public class SITESRepairMaterial
    {
        [JsonIgnore]
        public int SITESRepairMaterialId { get; set; }

        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }

        public int MaterialAmount { get; set; }

        [JsonIgnore]
        public int SITESBuildingId { get; set; }
        [JsonIgnore]
        public virtual SITESBuilding SITESBuilding { get; set; }
    }
}
