﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class FLIGHTSModel
    {
        [JsonIgnore]
        public int FLIGHTSModelId { get; set; }

        public virtual List<FLIGHTSFlight> Flights { get; set; } = new List<FLIGHTSFlight>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class FLIGHTSFlight
    {
        [JsonIgnore]
        public int FLIGHTSFlightId { get; set; }

        public string FlightId { get; set; }
        public string ShipId { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public virtual List<FLIGHTSFlightSegment> Segments { get; set; } = new List<FLIGHTSFlightSegment>();

        public int CurrentSegmentIndex { get; set; }

        public double StlDistance { get; set; }
        public double FtlDistance { get; set; }

        public bool IsAborted { get; set; }

        [JsonIgnore]
        public int FLIGHTSModelId { get; set; }

        [JsonIgnore]
        public virtual FLIGHTSModel FLIGHTSModel { get; set; }
    }

    public class FLIGHTSFlightSegment
    {
        [JsonIgnore]
        public int FLIGHTSFlightSegmentId { get; set; }

        public string Type { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public double? StlDistance { get; set; }
        public double? StlFuelConsumption { get; set; }
        public double? FtlDistance { get; set; }
        public double? FtlFuelConsumption { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }

        public virtual List<OriginLine> OriginLines { get; set; } = new List<OriginLine>();
        public virtual List<DestinationLine> DestinationLines { get; set; } = new List<DestinationLine>();

        [JsonIgnore]
        public int FLIGHTSFlightId { get; set; }

        [JsonIgnore]
        public virtual FLIGHTSFlight FLIGHTSFlight { get; set; }
    }

    public class OriginLine
    {
        [JsonIgnore]
        public int OriginLineId { get; set; }

        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }

        [JsonIgnore]
        public int FLIGHTSFlightSegmentId { get; set; }

        [JsonIgnore]
        public virtual FLIGHTSFlightSegment FLIGHTSFlightSegment { get; set; }
    }

    public class DestinationLine
    {
        [JsonIgnore]
        public int DestinationLineId { get; set; }

        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }

        [JsonIgnore]
        public int FLIGHTSFlightSegmentId { get; set; }

        [JsonIgnore]
        public virtual FLIGHTSFlightSegment FLIGHTSFlightSegment { get; set; }
    }
}
