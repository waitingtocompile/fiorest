﻿using System;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class MATModel
    {
        [JsonIgnore]
        public int MATModelId { get; set; }

        public string CategoryName { get; set; }
        public string CategoryId { get; set; }

        public string Name { get; set; }
        public string Id { get; set; }
        public string Ticker { get; set; }

        public double Weight { get; set; }
        public double Volume { get; set; }

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
