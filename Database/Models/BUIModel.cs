﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class BUIModel
    {
        public BUIModel() { }

        [JsonIgnore]
        public int BUIModelId { get; set; }

        public string Name { get; set; }
        public string Ticker { get; set; }

        public string Expertise { get; set; }

        public int Pioneers { get; set; }
        public int Settlers { get; set; }
        public int Technicians { get; set; }
        public int Engineers { get; set; }
        public int Scientists { get; set; }

        public int AreaCost { get; set; }

        public virtual List<BUIBuildingCost> BuildingCosts { get; set; } = new List<BUIBuildingCost>();
        public virtual List<BUIRecipe> Recipes { get; set; } = new List<BUIRecipe>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class BUIBuildingCost
    {
        public BUIBuildingCost() { }

        [JsonIgnore]
        public int BUIBuildingCostId { get; set; }

        public string CommodityName { get; set; }
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [JsonIgnore]
        public int BUIModelId { get; set; }

        [JsonIgnore]
        public virtual BUIModel BuidModel { get; set; }
    }

    public class BUIRecipeInput
    {
        public BUIRecipeInput() { }

        [JsonIgnore]
        public int BUIRecipeInputId { get; set; }

        public string CommodityName { get; set; }
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [JsonIgnore]
        public int BUIRecipeId { get; set; }
        [JsonIgnore]
        public virtual BUIRecipe BUIRecipe { get; set; }
    }

    public class BUIRecipeOutput
    {
        public BUIRecipeOutput() { }

        [JsonIgnore]
        public int BUIRecipeOutputId { get; set; }

        public string CommodityName { get; set; }
        public string CommodityTicker { get; set; }
        public double Weight { get; set; }
        public double Volume { get; set; }

        public int Amount { get; set; }

        [JsonIgnore]
        public int BUIRecipeId { get; set; }
        [JsonIgnore]
        public virtual BUIRecipe BUIRecipe { get; set; }
    }

    public class BUIRecipe
    {
        public BUIRecipe() { }

        [JsonIgnore]
        public int BUIRecipeId { get; set; }

        public virtual List<BUIRecipeInput> Inputs { get; set; } = new List<BUIRecipeInput>();
        public virtual List<BUIRecipeOutput> Outputs { get; set; } = new List<BUIRecipeOutput>();
        public int DurationMs { get; set; }

        [JsonIgnore]
        public int BUIModelId { get; set; }
        [JsonIgnore]
        public virtual BUIModel BUIModel { get; set; }
    }
}
