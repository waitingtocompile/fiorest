﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class StorageModel
    {
        [JsonIgnore]
        public int StorageModelId { get; set; }

        public string StorageId { get; set; }
        public string AddressableId { get; set; }
        public string Name { get; set; }
        public double WeightLoad { get; set; }
        public double WeightCapacity { get; set; }
        public double VolumeLoad { get; set; }
        public double VolumeCapacity { get; set; }

        public virtual List<StorageItem> StorageItems { get; set; } = new List<StorageItem>();

        public bool FixedStore { get; set; }
        public string Type { get; set; }

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class StorageItem
    {
        [JsonIgnore]
        public int StorageItemId { get; set; }

        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialCategory { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public string Type { get; set; }

        public double TotalWeight { get; set; }
        public double TotalVolume { get; set; }

        [JsonIgnore]
        public int StorageModelId { get; set; }
        [JsonIgnore]
        public virtual StorageModel StorageModel { get; set; }
    }
}
