﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class WorldSectorsModel
    {
        [JsonIgnore]
        public int WorldSectorsModelId { get; set; }

        public string SectorId { get; set; }
        public string Name { get; set; }

        public int HexQ { get; set; }
        public int HexR { get; set; }
        public int HexS { get; set; }

        public int Size { get; set; }

        public virtual List<SubSector> SubSectors { get; set; } = new List<SubSector>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SubSector
    {
        [JsonIgnore]
        public int SubSectorId { get; set; }

        public string Id { get; set; }

        public virtual List<SubSectorVertex> Vertices { get; set; } = new List<SubSectorVertex>();

        [JsonIgnore]
        public int WorldSectorsModelId { get; set; }
        [JsonIgnore]
        public virtual WorldSectorsModel WorldSectorsModel { get; set; }
    }

    public class SubSectorVertex
    {
        [JsonIgnore]
        public int SubSectorVertexId { get; set; }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        [JsonIgnore]
        public int SubSectorId { get; set; }
        [JsonIgnore]
        public virtual SubSector SubSector { get; set; }
    }
}
