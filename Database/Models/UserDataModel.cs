﻿using System;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class UserDataModel
    {
        [JsonIgnore]
        public int UserDataModelId { get; set; }

        public string UserName { get; set; }
        public string Tier { get; set; }

        public bool Team { get; set; }
        public bool Pioneer { get; set; }

        public int SystemNamingRights { get; set; }
        public int PlanetNamingRights { get; set; }

        public bool IsPayingUser { get; set; }
        public bool IsModeratorChat { get; set; }
        
        public long CreatedEpochMs { get; set; }

        public string CompanyId { get; set; }

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
