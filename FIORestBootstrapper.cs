﻿using System;

using Nancy;
using Nancy.Bootstrapper;
using Nancy.LeakyBucket;
using Nancy.Gzip;
using Nancy.TinyIoc;

namespace FIORest
{
    public class FIORestBootstrapper : DefaultNancyBootstrapper
    {
        public FIORestBootstrapper()
        {

        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            base.RequestStartup(container, pipelines, context);

            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
            {
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                                .WithHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
                                .WithHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

            });
        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            LeakyBucketRateLimiter.Enable(pipelines, new LeakyBucketRateLimiterConfiguration
            {
                MaxNumberOfRequests = 20,
                RefreshRate = TimeSpan.FromSeconds(1)
            });
            
            var gzipSettings = new GzipCompressionSettings();
            gzipSettings.MinimumBytes = 2048;
            pipelines.EnableGzipCompression(gzipSettings);

            base.ApplicationStartup(container, pipelines);

            pipelines.OnError.AddItemToEndOfPipeline((ctx, ex) =>
            {
                if (ctx.Request != null)
                {
                    return Utils.ReturnBadResponse(ctx.Request, ex);
                }

                return null;
            });
        }
    }
}
