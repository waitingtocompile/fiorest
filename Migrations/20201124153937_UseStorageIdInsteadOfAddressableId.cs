﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations
{
    public partial class UseStorageIdInsteadOfAddressableId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StorageId",
                table: "StorageModels",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StorageId",
                table: "StorageModels");
        }
    }
}
