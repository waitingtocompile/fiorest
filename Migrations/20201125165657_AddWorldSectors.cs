﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations
{
    public partial class AddWorldSectors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WorldSectorsModels",
                columns: table => new
                {
                    WorldSectorsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SectorId = table.Column<string>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    HexQ = table.Column<int>(type: "INTEGER", nullable: false),
                    HexR = table.Column<int>(type: "INTEGER", nullable: false),
                    HexS = table.Column<int>(type: "INTEGER", nullable: false),
                    Size = table.Column<int>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorldSectorsModels", x => x.WorldSectorsModelId);
                });

            migrationBuilder.CreateTable(
                name: "SubSectors",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    SubSectorId = table.Column<int>(type: "INTEGER", nullable: false),
                    WorldSectorsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubSectors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubSectors_WorldSectorsModels_WorldSectorsModelId",
                        column: x => x.WorldSectorsModelId,
                        principalTable: "WorldSectorsModels",
                        principalColumn: "WorldSectorsModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubSectorVertices",
                columns: table => new
                {
                    SubSectorVertexId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    X = table.Column<double>(type: "REAL", nullable: false),
                    Y = table.Column<double>(type: "REAL", nullable: false),
                    Z = table.Column<double>(type: "REAL", nullable: false),
                    SubSectorId = table.Column<int>(type: "INTEGER", nullable: false),
                    SubSectorId1 = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubSectorVertices", x => x.SubSectorVertexId);
                    table.ForeignKey(
                        name: "FK_SubSectorVertices_SubSectors_SubSectorId1",
                        column: x => x.SubSectorId1,
                        principalTable: "SubSectors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubSectors_WorldSectorsModelId",
                table: "SubSectors",
                column: "WorldSectorsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SubSectorVertices_SubSectorId1",
                table: "SubSectorVertices",
                column: "SubSectorId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubSectorVertices");

            migrationBuilder.DropTable(
                name: "SubSectors");

            migrationBuilder.DropTable(
                name: "WorldSectorsModels");
        }
    }
}
