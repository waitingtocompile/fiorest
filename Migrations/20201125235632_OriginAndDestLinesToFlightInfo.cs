﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations
{
    public partial class OriginAndDestLinesToFlightInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FLIGHTSDestinationLines",
                columns: table => new
                {
                    DestinationLineId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LineId = table.Column<string>(type: "TEXT", nullable: true),
                    LineNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    LineName = table.Column<string>(type: "TEXT", nullable: true),
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSDestinationLines", x => x.DestinationLineId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSDestinationLines_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                        column: x => x.FLIGHTSFlightSegmentId,
                        principalTable: "FLIGHTSFlightSegments",
                        principalColumn: "FLIGHTSFlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSOriginLines",
                columns: table => new
                {
                    OriginLineId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LineId = table.Column<string>(type: "TEXT", nullable: true),
                    LineNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    LineName = table.Column<string>(type: "TEXT", nullable: true),
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSOriginLines", x => x.OriginLineId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSOriginLines_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                        column: x => x.FLIGHTSFlightSegmentId,
                        principalTable: "FLIGHTSFlightSegments",
                        principalColumn: "FLIGHTSFlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSDestinationLines_FLIGHTSFlightSegmentId",
                table: "FLIGHTSDestinationLines",
                column: "FLIGHTSFlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSOriginLines_FLIGHTSFlightSegmentId",
                table: "FLIGHTSOriginLines",
                column: "FLIGHTSFlightSegmentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FLIGHTSDestinationLines");

            migrationBuilder.DropTable(
                name: "FLIGHTSOriginLines");
        }
    }
}
