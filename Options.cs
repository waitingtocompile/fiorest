﻿namespace FIORest
{
    public class Options
    {
        public int VerbosityLevel = 0;
        public bool OnlyHttp = false;
        public string BadRequestPath = "./BadRequests";
        public string DatabaseFilePath = "./prundata.db";
        public string CertFilePath = "./rest.pfx";
        public string CertPassword = null;
        public string UpdateDirectory = "FIOUIRelease";
    }
}
